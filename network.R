TransformaTextoEmDataframe = function(entrada){
  my_dbsource = "isi" 
  my_format = "plaintext" 
  my_papers_df <<- convert2df(entrada, dbsource=my_dbsource, format=my_format) #definir formato e font
  my_papers_df <<- metaTagExtraction(my_papers_df, Field = "AU_CO", sep = ";")
  my_papers_df <<- metaTagExtraction(my_papers_df, Field = "CR_AU", sep = ";")
  my_papers_df <<- metaTagExtraction(my_papers_df, Field = "CR_SO", sep = ";")
  my_papers_df <<- metaTagExtraction(my_papers_df, Field = "AU_UN", sep = ";")
  # VETOR DE TERMOS PARA EXTRAÇÃO
  my_keep.terms <<- c()
  # VETOR DE TERMOS A SER REMOVIDO
  my_remove.terms <<- c()
  # VETOR DE SINÔNIMOS
  my_synonyms <<- c("study; studies", "system; systems", "library;libraries", "user;users", "MODEL;MODELS" )
  
  my_papers_df <<- termExtraction(my_papers_df, Field = "TI",  remove.numbers=TRUE,  
                                  remove.terms=my_remove.terms, keep.terms=my_keep.terms, verbose=FALSE)
  my_papers_df <<- termExtraction(my_papers_df, Field = "AB",  remove.numbers=TRUE,
                                  remove.terms=my_remove.terms, keep.terms=my_keep.terms, verbose=FALSE)
  my_papers_df <<- termExtraction(my_papers_df, Field = "DE",  remove.numbers=TRUE,
                                  remove.terms=my_remove.terms, keep.terms=my_keep.terms, verbose=FALSE)
  my_papers_df <<- termExtraction(my_papers_df, Field = "ID",  remove.numbers=TRUE,
                                  remove.terms=my_remove.terms, keep.terms=my_keep.terms, verbose=FALSE)
}

CriaAnaliseBibliometrica = function() { 
  my_results <<- biblioAnalysis(my_papers_df, sep = ";")
}

QuantidadePublicacoes = function(){ length(my_papers_df[[1]]) }

TransformaDataframeEmGrafo = function(my_analysis, my_network, my_netDegree){
  my_NetMatrix <<-""
  my_graph <<-""
  if (my_network=="history references"){
    my_graph_his <-histNetwork(my_papers_df,sep = ";")
    my_NetMatrix <- my_graph_his$NetMatrix
  }
  else
  {
    my_NetMatrix <- biblioNetwork(my_papers_df, analysis = my_analysis, network = my_network, sep = ";")
  }
  diag <- Matrix::diag 
  my_NetMatrix <-as.matrix( my_NetMatrix[diag(my_NetMatrix) >= my_netDegree,diag(my_NetMatrix) >= my_netDegree])
  
  my_graph <<-graph_from_adjacency_matrix(my_NetMatrix, weighted=TRUE,mode = "directed")
  my_NetMatrix <<-my_NetMatrix
  my_graph.description <<- paste ("Network of ",my_analysis," of ",my_network, " with threshold ", my_netDegree,". With ", vcount(my_graph), " nodes and ", ecount(my_graph), " edges.",  sep = "")
  
}

CalculaMedidasCentralidade <- function(){
  my_graph_metrics<<-""
  my_graph_metrics<- data.frame(V(my_graph)$name)
  my_graph_metrics["name"]<- data.frame(V(my_graph)$name)
  
  # Grau, grau de entrada e grau de saída
  V(my_graph)$degree <- c(degree(my_graph))
  my_graph_metrics["degree"]<-(V(my_graph)$degree)
  my_graph.degree.summary <<- summary(V(my_graph)$degree)
  my_graph.degree.sd <<-sd(V(my_graph)$degree)
  my_graph.degree.var <<-var(V(my_graph)$degree)
  
  # Grau de entrada
  V(my_graph)$indegree <- c(degree(my_graph, mode = "in"))
  my_graph_metrics["indegree"]<-(V(my_graph)$indegree)
  my_graph.indegree.summary <<-summary(V(my_graph)$indegree)
  my_graph.indegree.sd <<-sd(V(my_graph)$indegree)
  my_graph.indegree.var <<-var(V(my_graph)$indegree)
  
  # Grau de saida 
  V(my_graph)$outdegree <- c(degree(my_graph, mode = "out"))
  my_graph_metrics["outdegree"] <- (V(my_graph)$outdegree)
  my_graph.outdegree.summary <<-summary(V(my_graph)$outdegree)
  my_graph.outdegree.sd <<-sd(V(my_graph)$outdegree)
  my_graph.outdegree.var <<-var(V(my_graph)$outdegree)
  my_graph_metrics<<-my_graph_metrics
  # 4.2 Força dos vértices
  # Força = força de entrada + força de saída
  V(my_graph)$strength <- c(graph.strength(my_graph))
  my_graph_metrics["strength"]<- (V(my_graph)$strength)
  my_graph.strength.summary <<-summary(V(my_graph)$strength)
  my_graph.strength.sd <<-sd(V(my_graph)$strength)
  
  # Força de entrada
  V(my_graph)$instrength <- c(graph.strength(my_graph, mode =c("in")))
  my_graph_metrics["instrength"]<- (V(my_graph)$instrength)
  my_graph.instrength.summary <<-summary(V(my_graph)$instrength )
  my_graph.instrength.sd <<-sd(V(my_graph)$instrength )
  my_graph.instrength.var <<-var(V(my_graph)$instrength)
  
  # Força de saída
  V(my_graph)$outstrength <- c(graph.strength(my_graph, mode =c("out")))
  my_graph_metrics["outstrength"]<- (V(my_graph)$outstrength)
  my_graph.outstrength.summary <<-summary(V(my_graph)$outstrength)
  my_graph.outstrength.sd <<-sd(V(my_graph)$outstrength)
  my_graph_metrics<<-my_graph_metrics   #### ?????????
  
  # 4.7 log log
  my_graph.degree.distribution <<- c(degree.distribution(my_graph))
  my_d <<- 1:max(V(my_graph)$degree)-1
  my_ind <<- (V(my_graph)$degree.distribution != 0) 
  
  #4.8 - knn Calculate the average nearest neighbor degree of the given vertices and the same quantity in the function of vertex degree
  #my_graph.a.nn.deg <<- graph.knn(my_graph,V(my_graph))$knn
  
  # Diameter - distância geodesica
  my_graph.diameter <<-diameter(my_graph, directed = TRUE, unconnected=TRUE, weights = NULL)
  
  ##Retorna os caminho com diametro atual
  my_graph.get_diameter <<-get_diameter(my_graph)
  
  ##Retorna os 2 vértices que são conectados pelo diâmetro
  my_graph.farthest_vertices <<-farthest_vertices(my_graph)
  
  # 4.Proximidade - A centralidade de proximidade mede quantas etapas são necessárias para acessar cada outro vértice de um determinado vértice.
  V(my_graph)$closeness <- c(closeness(my_graph))
  my_graph_metrics["closeness"]<<-(V(my_graph)$closeness)
  my_graph.closeness <<- centralization.closeness(my_graph)
  my_graph.closeness.res.summary <<- summary (my_graph.closeness$res)
  my_graph.closeness.res.sd <<-sd(my_graph.closeness$res)
  
  # 4.Intermediação
  V(my_graph)$betweenness <- betweenness(my_graph)
  my_graph_metrics["betweenness"]<<- (V(my_graph)$betweenness)
  my_graph.betweenness <<- centralization.betweenness(my_graph)
  my_graph.betweenness.res.summary <<-summary (my_graph.betweenness$res)
  my_graph.betweenness.res.sd <<-sd(my_graph.betweenness$res)
  
  # 4.Excentricidade
  V(my_graph)$eccentricity <-eccentricity(my_graph)
  my_graph_metrics["eccentricity"]<<- (V(my_graph)$eccentricity)
  my_graph.eccentricity.res.summary <<- summary (V(my_graph)$eccentricity)
  my_graph.eccentricity.res.sd <<-sd(V(my_graph)$eccentricity)
  
  # 4.eigen_centrality
  my_graph.eigen <<-eigen_centrality(my_graph)
  V(my_graph)$eigen <-(eigen_centrality(my_graph))$vector
  my_graph_metrics["eigen"] <<-  (V(my_graph)$eigen)
  my_graph_metrics.eigen.summary <<-summary(V(my_graph)$eigen)
  my_graph_metrics.eigen.summary.sd <<-sd(V(my_graph)$eigen)
  
  
  # 4.Densidade
  my_graph.density <<-graph.density(my_graph)
  
  # Modularidade
  wtc <- cluster_walktrap(my_graph)
  #modularity(wtc)
  my_graph.modularity <<-modularity(my_graph, membership(wtc))
  my_graph.modularity.matrix <<-modularity_matrix(my_graph,membership(wtc))
  
  # Page Rank
  V(my_graph)$pagerank <-(page.rank(my_graph))$vector
  my_graph_metrics["pagerank"]<<- (V(my_graph)$pagerank)
  my_graph_metrics.pagerank.summary <<-summary(V(my_graph)$pagerank)
  my_graph_metrics.pagerank.summary.sd <<-sd(V(my_graph)$pagerank)
  
  
  # Clustering
  my_graph_clusters <<-clusters(my_graph, mode="strong" )
  V(my_graph)$clustering <-(clusters(my_graph))$membership
  my_graph_metrics["clustering"] <<-  (V(my_graph)$clustering)
  my_graph_metrics.clustering.summary <<-summary(V(my_graph)$clustering)
  my_graph_metrics.clustering.summary.sd <<-sd(V(my_graph)$clustering)
  
}

ImprimeGrau = function(){
  #
  # GRAU DOS VERTICES
  hist(my_graph.degree,col="lightblue",xlim=c(0, max(my_graph.degree)),xlab="Grau dos vértices", ylab="Frequência", main="", axes="TRUE")
  legend("topright", c(paste("Mínimo =", round(my_graph.degree.summary[1],2)), 
                       paste("Máximo=", round(my_graph.degree.summary[6],2)), 
                       paste("Média=", round(my_graph.degree.summary[4],2)),
                       paste("Mediana=", round(my_graph.degree.summary[3],2)),
                       paste("Desvio Padrão=", round(my_graph.degree.sd[1],2))),
         pch = 1, title = "Grau")
  
  # GRAU DE ENTRADA
  hist(my_graph.indegree,col="lightblue", xlab="Grau de entrada", ylab="Frequência", main="", axes="TRUE")
  legend("topright", c(paste("Mínimo =", round(my_graph.indegree.summary[1],2)), 
                       paste("Máximo=", round(my_graph.indegree.summary[6],2)), 
                       paste("Média=", round(my_graph.indegree.summary[4],2)),
                       paste("Mediana=", round(my_graph.indegree.summary[3],2)),
                       paste("Desvio Padrão=", round(my_graph.indegree.sd[1],2))),
         pch = 1, title = "Grau entrada")
  
  # GRAU DE SAÍDA
  hist(my_graph.outdegree,col="lightblue", xlab="Grau de saída", ylab="Frequência", main="", axes="TRUE")
  legend("topright", c(paste("Mínimo =", round(my_graph.outdegree.summary[1],2)), 
                       paste("Máximo=", round(my_graph.outdegree.summary[6],2)), 
                       paste("Média=", round(my_graph.outdegree.summary[4],2)),
                       paste("Mediana=", round(my_graph.outdegree.summary[3],2)),
                       paste("Desvio Padrão=", round(my_graph.outdegree.sd[1],2))),
         pch = 1, title = "Grau saída")
  
  # BOXPLOT: GRAU ENTRADA, GRAU SAÍDA E GRAU TOTAL
  boxplot(my_graph.indegree, my_graph.outdegree, my_graph.degree, notch = FALSE, ylab = 'Grau', 
          names = c('Grau entrada', 'Grau saída', 'Grau total'), 
          main = 'Boxplot do grau dos vértices', col = c('blue', 'red', 'orange'),shrink=0.8, textcolor="red")
  
}
