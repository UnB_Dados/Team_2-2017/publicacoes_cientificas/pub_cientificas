source("statistics.R")
source("bibliometrics.R")
source("network.R")

### formata numeros com decimais m informa numero e qtd de casas decimais apos a virgula 
fmtd = function(n,d){ prettyNum(round(n,d), big.mark = ".", big.interval = 3, decimal.mark = ",", nsmall = 2) }

shinyServer(
  function(input, output,session) {

    ### Upload File
    # uploadData <- reactive({
    #   if (is.null(input$arqtrab))
    #     return(NULL)
    #   else input$arqtrab
    # })
    # output$dadosArquivo = renderText({
    #   arquivoEntrada <- uploadData()
    #   observacoes = readLines(arquivoEntrada$datapath, encoding = getOption("encoding"))
    #   TransformaTextoEmDataframe(observacoes)
    #   CriaAnaliseBibliometrica()
    #   print(paste("Total de Publicações: ", QuantidadePublicacoes()))
    # })
    ### original
    output$dadosArquivo = renderText({
      arquivoEntrada <- input$arqtrab
      if (is.null(arquivoEntrada))
        return(NULL)
      observacoes = readLines(arquivoEntrada$datapath, encoding = getOption("encoding"))
      TransformaTextoEmDataframe(observacoes)
      CriaAnaliseBibliometrica()
      print(paste("Total de Publicações: ", QuantidadePublicacoes()))
    })

        
    # Analise Bibliometrica
    observeEvent(input$show0, {
      hide("nuvem_de_palavras_chave")
      show("painel_1")    
      output$painel_1 = renderDataTable(
        
        PublicacoesMaisReferenciadas() 
        , options = list(lengthMenu = c(10, 10, 50,100), pageLength = 10)
      )
    })
    
    observeEvent(input$show1, {
      hide("nuvem_de_palavras_chave")
      show("painel_1")    
      output$painel_1 =  renderDataTable(
        AutoresMaisCitados()
        , options = list(lengthMenu = c(10, 10, 50,100), pageLength = 10) 
      )
    })  
    
    observeEvent(input$show2, {
      hide("nuvem_de_palavras_chave")
      show("painel_1")  
      output$painel_1 =  renderDataTable(
        PalavrasChavesMaisUtilizadas()  
        , options = list(lengthMenu = c(10, 10, 50,100), pageLength = 10) 
      )
    })  
    
    observeEvent(input$show3, {
      hide("nuvem_de_palavras_chave")
      show("painel_1")  
      output$painel_1 =  renderDataTable(
        LinguasPublicacao()
        , options = list(lengthMenu = c(10, 10, 50,100), pageLength = 10) 
      )
    })  
    
    observeEvent(input$show4, {
      hide("nuvem_de_palavras_chave")
      show("painel_1")  
      output$painel_1 =  renderDataTable(
        PeriodicosQueMaisPublicam()
        , options = list(lengthMenu = c(10, 10, 50,100), pageLength = 10) 
      )
    })    
    
    observeEvent(input$show5, {
      hide("nuvem_de_palavras_chave")
      show("painel_1")
      output$painel_1 =  renderDataTable(
        TiposDePublicacao()
        , options = list(lengthMenu = c(10, 10, 50,100), pageLength = 10) 
      )
    })  
    
    observeEvent(input$show6, {
      hide("painel_1")
      show("nuvem_de_palavras_chave")
      output$nuvem_de_palavras_chave =  renderPlot({ 
        
        CriaNuvemDePalavras() 
      })
    })  
    # Fim Analise Bibliometrica
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ### CRIAR REDE    
    observeEvent(input$in_cria_rede,{
      output$out_table_graph = renderDataTable({
        dd <-my_graph_metrics[c("name","degree","indegree","outdegree")]
        dd[order(-dd$degree),]
      }, options = list(lengthMenu = c(10, 20, 50,100), pageLength = 10))
    })
    
    observeEvent(input$in_cria_rede, {
      output$out_plot_net = renderPlot({
        if (isolate(input$in_tp_analysis)=="Colaboração Autores"){
          my_analysis <- "collaboration"
          my_network <- "authors"
        }
        if (isolate(input$in_tp_analysis)=="Colaboração Países"){
          my_analysis <- "collaboration"
          my_network <- "countries"
        }
        if (isolate(input$in_tp_analysis)=="Colaboração Instituições"){
          my_analysis <- "collaboration"
          my_network <- "universities"
        }
        if (isolate(input$in_tp_analysis)=="Citação Documetos"){
          my_analysis <- "coupling"
          my_network <- "references"
        }
        if (isolate(input$in_tp_analysis)=="Citação Autores"){
          my_analysis <- "coupling"
          my_network <- "authors"
        }
        if (isolate(input$in_tp_analysis)=="Citação Fontes"){
          my_analysis <- "coupling"
          my_network <- "sources"
        }
        if (isolate(input$in_tp_analysis)=="Citação Países"){
          my_analysis <- "coupling"
          my_network <- "countries"
        }
        if (isolate(input$in_tp_analysis)=="Co-citação Documentos"){
          my_analysis <- "co-citation"
          my_network <- "references"
        }
        if (isolate(input$in_tp_analysis)=="Co-citação Autores"){
          my_analysis <- "co-citation"
          my_network <- "authors"
        }
        if (isolate(input$in_tp_analysis)=="Co-citação Fontes"){
          my_analysis <- "co-citation"
          my_network <- "sources"
        }
        if (isolate(input$in_tp_analysis)=="Co-citação Histórico Referências"){
          my_analysis <- "co-citation"
          my_network <- "history references"
        }
        if (isolate(input$in_tp_analysis)=="Co-ocorrência Autores"){
          my_analysis <- "co-occurrences"
          my_network <- "authors"
        }
        if (isolate(input$in_tp_analysis)=="Co-ocorrência Fontes"){
          my_analysis <- "co-occurrences"
          my_network <- "sources"
        }
        if (isolate(input$in_tp_analysis)=="Co-ocorrência Palavra-chave"){
          my_analysis <- "co-occurrences"
          my_network <- "keywords"
        }
        if (isolate(input$in_tp_analysis)=="Co-ocorrência Palavra-chave autor"){
          my_analysis <- "co-occurrences"
          my_network <- "author_keywords"
        }
        if (isolate(input$in_tp_analysis)=="Co-ocorrência Título"){
          my_analysis <- "co-occurrences"
          my_network <- "titles"
        }
        if (isolate(input$in_tp_analysis)=="Co-ocorrência Resumo"){
          my_analysis <- "co-occurrences"
          my_network <- "abstracts"
        }
        
        my_analysis <<- my_analysis
        my_network <<- my_network
        my_netDegree <<- isolate(input$in_num_freq)
        TransformaDataframeEmGrafo(my_analysis, my_network, my_netDegree)
        CalculaMedidasCentralidade()
        
        
        # if (isolate(input$in_st_exibe_diametro)==TRUE){
        #   if (length(diameter(my_graph)>=2)){
        #     my_graph.nodes.diameter<-get.diameter(my_graph)
        #     V(my_graph)[my_graph.nodes.diameter]$color<-"darkgreen"
        #     V(my_graph)[my_graph.nodes.diameter]$size<-10
        #     V(my_graph)[my_graph.nodes.diameter]$label.color<-"white"
        #     E(my_graph)$color<-"grey"
        #     E(my_graph,my_graph.nodes.diameter)$color<-"darkgreen"
        #     E(my_graph,my_graph.nodes.diameter)$width<-2
        #     #Edges in the diameter will be darkgreen and a little extra wide
        #   }
        # }
        # else{
        #   E(my_graph)$color<-"grey"
        # }
        
        # karate_groups <- cluster_optimal(karate)
        # coords <- layout_in_circle(karate, order = order(membership(karate_groups)))
        # V(karate)$label <- sub("Actor ", "", V(karate)$name)
        # V(karate)$label.color <- membership(karate_groups)
        # V(karate)$shape <- "none"
        # plot(karate, layout = coords)
        
        if (isolate (input$in_tp_linha)==TRUE){
          E(my_graph)$width <- E(my_graph)$weight/mean(E(my_graph)$weight)
          my_graph.description <<-paste (my_graph.description, " Edges width with weight.", sep = "")
        }
        else{
          E(my_graph)$width <- 1
        }
        
        if (isolate (input$in_tp_dim)=="in"){
          V(my_graph)$size<-my_graph_metrics$indegree * 0.5  
          my_graph.description <<- paste (my_graph.description, " Dimension node with indegree.",  sep = "")
        }
        else if (isolate (input$in_tp_dim)=="out"){
          V(my_graph)$size<--my_graph_metrics$outdegree * 0.5 
          my_graph.description <<- paste (my_graph.description, "Dimension node with outdegree.",sep = "")
        }
        else {
          V(my_graph)$size<-my_graph_metrics$degree * 0.5 
          my_graph.description <<- paste (my_graph.description, " Dimension node with degree.", sep = "")
        }
        
        if (isolate (input$in_tp_cluster)==TRUE){
          V(my_graph)$color <- rainbow(my_graph_clusters$no)[my_graph_clusters$membership]
          my_graph.description <<- paste (my_graph.description, " Number of clusters:", my_graph_clusters$no, ".", sep = "")
        }
        else{
          V(my_graph)$color <- "orange"
        }
        
        if (isolate (input$in_tp_gigante)==TRUE){
          comp_gigante = which(my_graph_clusters$membership == which.max(my_graph_clusters$csize))
          V(my_graph)[comp_gigante]$color = "#00FFFF"
        }
        
        coords <- as.matrix(layout_(my_graph, as_star()))
        my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        
        if (isolate (input$in_tp_layout)=="star"){
          coords <- as.matrix(layout_(my_graph, as_star()))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        if (isolate (input$in_tp_layout)=="tree"){
          coords <-as.matrix(layout_as_tree(my_graph, circular=TRUE))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        if (isolate (input$in_tp_layout)=="circle"){ ## coords <- layout_in_circle(karate, order = order(membership(karate_groups)))
          coords <- as.matrix( layout_in_circle(my_graph))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        if (isolate (input$in_tp_layout)=="mds"){ #layout_with_mds(g) layout_nicely(graph, dim = 2, ...)
          coords <- as.matrix( layout_with_mds(my_graph))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        if (isolate (input$in_tp_layout)=="nicely"){ #layout_nicely(graph, dim = 2, ...
          coords <- as.matrix( layout_nicely(my_graph))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        if (isolate (input$in_tp_layout=="With_dh")){ #plot(g_8, layout=layout_with_dh, vertex.size=5, vertex.label=NA)
          coords <- as.matrix( layout_with_dh(my_graph))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        if (isolate (input$in_tp_layout)=="with_gem"){ #plot(g, layout=layout_with_gem)
          coords <- as.matrix( layout_with_gem(my_graph))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        if (isolate (input$in_tp_layout)=="on_grid"){ # plot(g, layout=layout_on_grid) rglplot(g, layout=layout_on_grid(g, dim = 3))
          coords <- as.matrix(layout_on_grid(my_graph))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        if (isolate (input$in_tp_layout)=="layout.kamada.kawai"){ ## plot(g, layout=layout_with_kk, edge.label=E(g)$weight)
          coords <- as.matrix( layout_with_kk(my_graph))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        if (isolate (input$in_tp_layout)=="Fruchterman-Reingold"){
          coords <- as.matrix(layout.fruchterman.reingold(my_graph))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        if (isolate (input$in_tp_layout)=="layout.sphere"){
          coords <- as.matrix(layout_on_sphere(my_graph))
          my_coords<-norm_coords(coords, xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1)
        }
        
        my_graph.description <<- paste (my_graph.description, " Layout ",isolate (input$in_tp_layout), ".",sep = "")
        #dev.off()
        plot.new()
        par(mar=c(.1,.1,.1,.1))
        plot(my_graph,
             edge.arrow.size=0.5, 
             #edge.color = "grey",
             edge.curved= 0,
             layout = my_coords,#layout.fruchterman.reingold,
             #vertex.color=V(my_graph)$color,
             vertex.frame.color = 'black',
             vertex.label.dist = 0,
             #vertex.size = V(my_graph)$size,
             vertex.label.color = 'black',
             vertex.label.font = 15, 
             #vertex.label = V(my_graph)$name, 
             vertex.label.cex = 0.5)
      })
    })
    
    ## ESTATISTICAS DE REDE 
    
    # Eventos geradores de gráficos
    observeEvent(input$in_tp_metrica,{ 
      ## GRAU DA REDE 
      if (input$in_tp_metrica=="netdegree"){ 
        RestauraSaidasEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_plot")
        
        output$out_plot_statistics <- renderPlot({
          
          hist(my_graph_metrics$degree,col="lightblue",xlim=c(0, max(my_graph_metrics$degree)),xlab="Grau dos vértices", ylab="Frequência", main="", axes="TRUE")
          legend("topright", c(paste("Mín.=", round(my_graph.degree.summary[1],2)),
                               paste("Máx.=", round(my_graph.degree.summary[6],2)),
                               paste("Média=", round(my_graph.degree.summary[4],2)),
                               paste("Mediana=", round(my_graph.degree.summary[3],2)),
                               paste("D. padrão=", round(my_graph.degree.sd[1],2))),
                 pch = 1, title = "Grau")
          
        })
      }
      
      ## GRAU PONDERADO DA REDE
      if (input$in_tp_metrica=="strength"){ 
        RestauraSaidasEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_plot")
        output$out_plot_statistics <- renderPlot({
          
          hist(my_graph_metrics$strength,col="lightblue",xlim=c(0, max(my_graph_metrics$strength)),xlab="Grau Ponderado dos vértices", ylab="Frequência", main="", axes="TRUE")
          legend("topright", c(paste("Mín.=", round(my_graph.strength.summary[1],2)),
                               paste("Máx.=", round(my_graph.strength.summary[6],2)),
                               paste("Média=", round(my_graph.strength.summary[4],2)),
                               paste("Mediana=", round(my_graph.strength.summary[3],2)),
                               paste("D. padrão=", round(my_graph.strength.sd[1],2))),
                 pch = 1, title = "Grau Ponderado")
          
        })
      }
      
      
      if (input$in_tp_metrica=="pagerank"){ 
        RestauraSaidasEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_plot")
        output$out_plot_statistics <- renderPlot({
          
          hist(my_graph_metrics$pagerank,col="lightblue",xlim=c(0, max(my_graph_metrics$pagerank)),xlab="Pagerank", ylab="Frequência", main="", axes="TRUE")
          legend("topright", c(paste("Mín.=", round(my_graph_metrics.pagerank.summary[1],4)),
                               paste("Máx.=", round(my_graph_metrics.pagerank.summary[6],4)),
                               paste("Média=", round(my_graph_metrics.pagerank.summary[4],4)),
                               paste("Mediana=", round(my_graph_metrics.pagerank.summary[3],4))
                               ,
                               paste("D. padrão=", round(my_graph_metrics.pagerank.summary.sd[1],4))
          ),
          pch = 1, title = "Pagerank")
          
        })
      }
      
      
      
      if (input$in_tp_metrica=="clustering"){ 
        RestauraSaidasEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_plot")
        output$out_plot_statistics <- renderPlot({
          
          hist(my_graph_metrics$clustering,col="lightblue",xlim=c(0, max(my_graph_metrics$clustering)),xlab="Clustering", ylab="Frequência", main="", axes="TRUE")
          
          legend("topright", c(paste("Mín.=", round(my_graph_metrics.clustering.summary[1],4)),
                               paste("Máx.=", round(my_graph_metrics.clustering.summary[6],4)),
                               paste("Média=", round(my_graph_metrics.clustering.summary[4],4)),
                               paste("Mediana=", round(my_graph_metrics.clustering.summary[3],4))
                               ,
                               paste("D. padrão=", round(my_graph_metrics.clustering.summary.sd[1],4))
          ),
          pch = 1, title = "Clustering")
          
        })
      }
      
      
      if (input$in_tp_metrica=="eigen"){ 
        RestauraSaidasEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_plot")
        output$out_plot_statistics <- renderPlot({
          hist(my_graph_metrics$eigen,col="lightblue",xlim=c(0, max(my_graph_metrics$eigen)),xlab="Centralidade", ylab="Frequência", main="", axes="TRUE")
          
          
          legend("topright", c(paste("Mín.=", round(my_graph_metrics.eigen.summary[1],4)),
                               paste("Máx.=", round(my_graph_metrics.eigen.summary[6],4)),
                               paste("Média=", round(my_graph_metrics.eigen.summary[4],4)),
                               paste("Mediana=", round(my_graph_metrics.eigen.summary[3],4))
                               ,
                               paste("D. padrão=", round(my_graph_metrics.eigen.summary.sd[1],4))
          ),
          pch = 1, title = "Centralidade")
          
        })
      }
      
      if (input$in_tp_metrica=="betweenness"){ 
        RestauraSaidasEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_plot")
        output$out_plot_statistics <- renderPlot({
          hist(my_graph_metrics$betweenness,col="lightblue",xlim=c(0, max(my_graph_metrics$betweenness)),xlab="Centralidade", ylab="Frequência", main="", axes="TRUE")
          
          
          legend("topright", c(paste("Mín.=", round(my_graph.betweenness.res.summary[1],4)),
                               paste("Máx.=", round(my_graph.betweenness.res.summary[6],4)),
                               paste("Média=", round(my_graph.betweenness.res.summary[4],4)),
                               paste("Mediana=", round(my_graph.betweenness.res.summary[3],4))
                               ,
                               paste("D. padrão=", round(my_graph.betweenness.res.sd[1],4))
          ),
          pch = 1, title = "Centralidade")
          
        })
      }
      
      
      if (input$in_tp_metrica=="eccentricity"){ 
        RestauraSaidasEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_plot")
        output$out_plot_statistics <- renderPlot({
          hist(my_graph_metrics$eccentricity,col="lightblue",xlim=c(0, max(my_graph_metrics$eccentricity)),xlab="Excentricidade", ylab="Frequência", main="", axes="TRUE")
          
          
          legend("topright", c(paste("Mín.=", round(my_graph.eccentricity.res.summary[1],4)),
                               paste("Máx.=", round(my_graph.eccentricity.res.summary[6],4)),
                               paste("Média=", round(my_graph.eccentricity.res.summary[4],4)),
                               paste("Mediana=", round(my_graph.eccentricity.res.summary[3],4))
                               ,
                               paste("D. padrão=", round(my_graph.eccentricity.res.sd[1],4))
          ),
          pch = 1, title = "Excentricidade")
          
        })
      }    
      
      if (input$in_tp_metrica=="closeness"){ 
        RestauraSaidasEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_plot")
        output$out_plot_statistics <- renderPlot({
          hist(my_graph_metrics$closeness,col="lightblue",xlim=c(0, max(my_graph_metrics$closeness)),xlab="Proximidade", ylab="Frequência", main="", axes="TRUE")
          
          
          legend("topright", c(paste("Mín.=", round(my_graph.closeness.res.summary[1],4)),
                               paste("Máx.=", round(my_graph.closeness.res.summary[6],4)),
                               paste("Média=", round(my_graph.closeness.res.summary[4],4)),
                               paste("Mediana=", round(my_graph.closeness.res.summary[3],4))
                               ,
                               paste("D. padrão=", round(my_graph.closeness.res.sd[1],4))
          ),
          pch = 1, title = "Proximidade")
          
        })
      }
      
      
      # Eventos geradores de Informações  
      #Diametro 
      if (input$in_tp_metrica=="diameter"){ 
        EscondePlotEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_info")
        output$out_text_statistics = renderText(
          
          print(paste("<br><br><b>Diametro da Rede: </b>", my_graph.diameter))
        )
      }
      
      
      #Densidade da rede  
      if (input$in_tp_metrica=="density"){ 
        EscondePlotEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_info")
        
        
        output$out_text_statistics = renderText(
          
          print(paste("<br><br><b>Densidade da Rede: </b>", my_graph.density))
        )
      }
      
      
      #modularity 
      if (input$in_tp_metrica=="modularity"){ 
        EscondePlotEstatisticas()
        updateTabsetPanel(session,"estatisticasTabset" , selected = "tab_estatisticas_info")
        
        output$out_text_statistics = renderText(
          print(paste("<br><br><b>Modularidade da Rede: </b>", my_graph.modularity))
        )
      } 
      
      
      
    })
    
    
    
    # Eventos geradores de tabelas 
    observeEvent(input$in_tp_metrica,{
      ## GRAU DA REDE 
      if(input$in_tp_metrica == "netdegree") { 
        output$out_table_metrics = renderDataTable({
          dd <-my_graph_metrics[c("name","degree","indegree","outdegree")]
          dd[order(-dd$degree),]
        }, options = list(lengthMenu = c(10, 20, 50,100), pageLength = 10))
      }
      
      ## GRAU PONDERADO DA REDE
      if(input$in_tp_metrica == "strength") { 
        output$out_table_metrics = renderDataTable({
          dd <-my_graph_metrics[c("name","strength","instrength","outstrength")]
          dd[order(-dd$strength),]
        }, options = list(lengthMenu = c(10, 20, 50,100), pageLength = 10))
      }
      
      if(input$in_tp_metrica == "pagerank") { 
        output$out_table_metrics = renderDataTable({
          
          dd <-my_graph_metrics[c("name","pagerank")]
          
          dd[order(-dd$pagerank),]
        }, options = list(lengthMenu = c(10, 20, 50,100), pageLength = 10))
      }
      
      if(input$in_tp_metrica == "clustering") { 
        output$out_table_metrics = renderDataTable({
          
          dd <-my_graph_metrics[c("name","clustering")]
          
          dd[order(-dd$clustering),]
        }, options = list(lengthMenu = c(10, 20, 50,100), pageLength = 10))
      }
      
      
      if(input$in_tp_metrica == "eigen") { 
        output$out_table_metrics = renderDataTable({
          
          dd <-my_graph_metrics[c("name","eigen")]
          
          dd[order(-dd$eigen),]
        }, options = list(lengthMenu = c(10, 20, 50,100), pageLength = 10))
      }
      
      
      if(input$in_tp_metrica == "betweenness") { 
        output$out_table_metrics = renderDataTable({
          
          dd <-my_graph_metrics[c("name","betweenness")]
          
          dd[order(-dd$betweenness),]
        }, options = list(lengthMenu = c(10, 20, 50,100), pageLength = 10))
      }
      
      if(input$in_tp_metrica == "eccentricity") { 
        output$out_table_metrics = renderDataTable({
          
          dd <-my_graph_metrics[c("name","eccentricity")]
          
          dd[order(-dd$eccentricity),]
        }, options = list(lengthMenu = c(10, 20, 50,100), pageLength = 10))
      }
      
      if(input$in_tp_metrica == "closeness") { 
        output$out_table_metrics = renderDataTable({
          
          dd <-my_graph_metrics[c("name","closeness")]
          
          dd[order(-dd$closeness),]
        }, options = list(lengthMenu = c(10, 20, 50,100), pageLength = 10))
      }
      
    })

  
})
