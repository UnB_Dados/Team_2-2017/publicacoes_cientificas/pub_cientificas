###  Analise Bibliomtrica
PublicacoesMaisReferenciadas = function (qtd=11){
  print("Inicio PublicacaoesMaisReferenciadas")
  CR <- citations(my_papers_df, field = "article", sep = ".  ")
  publicacoes = capture.output(CR$Cited[2:qtd])
  publicacao <- c()
  qtd_citacoes <- c() 
  i=1
  saida = ""
  j=2
  while(j <= length(publicacoes)){
    publicacao <- append(publicacao, publicacoes[j] )
    qtd_citacoes <- append(qtd_citacoes , publicacoes[j+1])
    print(i)
    j=j+2
    i=i+1
  }
  
  saida <- data.frame(publicacao, qtd_citacoes)
  names(saida) <- c("Publicação" , "Quantidade de Citações") 
  print("Fim PublicacaoesMaisReferenciadas")
  
  saida
  
}

# AUTORES MAIS CITADOS
AutoresMaisCitados = function(n=10,ano="Todos"){
  
  if(ano != "Todos") {autores = my_papers_df$AU[my_papers_df$PY==ano]}
  else {autores = my_papers_df$AU}
  vautores = c()
  print("Arrumando autores")
  for(i in autores){
    
    for(j in strsplit(i, ";"))   vautores = c(vautores, trimws(j))
  }
  print("Fim autores")
  t=sort(table(vautores), decreasing = TRUE)
  autores = names(t)
  qtds = as.vector(t)
  
  saida <- data.frame(autores,qtds) 
  names(saida) <- c( "Autores" , "Numero de Citações")
  
  saida
}


PalavrasChavesMaisUtilizadas = function(n=10, ano = "Todos"){
  print("Inicio PalavrasChavesMaisUtilizadas")
  pc = c()
  analise = my_papers_df$DE
  if(ano != "Todos"){
    analise = my_papers_df$DE[my_papers_df$PY == ano]
  }
  if(n == "Todos"){
    n = length(my_papers_df$PY)
  }
  for(i in analise){
    for(j in strsplit(i, ";")){
      pc = c(pc, trimws(j))
    }
  }
  pc = pc[pc != ""]
  x= sort(table(pc), decreasing = TRUE)
  pc = names(x)
  valores = c(as.vector(x))
  saida <- data.frame(pc,valores)
  names(saida) <- c("Palavras Chave" , "Quantidade de utilizações")
  print("Fim PalavrasChavesMaisUtilizadas")
  
  saida
}

LinguasPublicacao = function(){
  print("Inicio LinguasPublicacao")
  x= sort(table (my_papers_df$LA), decreasing = TRUE)
  linguas = c(names(x))
  valores = c(as.vector(x))
  
  saida <- data.frame(linguas,valores)
  names(saida) <- c("Lingua" , "Quantidade de Publicações")
  print("Fim LinguasPublicacao")
  saida
}

# LISTA PERODICOS QUE MAIS PUBLICAM
PeriodicosQueMaisPublicam = function(n,ano="Todos"){
  print("Inicio Periodico")
  if(ano != "Todos") {periodicos = my_papers_df$PU[my_papers_df$PY==ano]}
  else {periodicos = my_papers_df$PU}
  t=sort(table(periodicos), decreasing = TRUE)
  nomes = names(t)
  qtds = as.vector(t)
  
  saida <- data.frame(nomes,qtds)
  names(saida) <- c("Periódico" , "Quantidade de Publicações")
  print("Fim Periodico")
  
  saida
}

TiposDePublicacao = function(){
  print("Inicio de TiposDePublicacao")
  x= sort(table (my_papers_df$DT), decreasing = TRUE)
  tiposDePublicacao = c(names(x))
  valores = c(as.vector(x))
  
  saida <- data.frame(tiposDePublicacao,valores)
  names(saida) <- c("Tipo de Publicacao" , "Quantidade")
  print("Fim de TiposDePublicacao")
  
  
  saida
}

CriaNuvemDePalavras = function() {
  #x= sort(table (my_papers_df$LA), decreasing = TRUE)
  d =   my_papers_df$DE[!is.na(my_papers_df$DE)] 
  conjunto_palavras = Corpus(VectorSource(d))
  
  
  tdm <- TermDocumentMatrix(conjunto_palavras)
  matrix <- as.matrix(tdm)
  v <- sort(rowSums(matrix) , decreasing=TRUE )
  d <- data.frame(word = names(v) , freq=v)
  
  saida <- wordcloud(d$word , d$freq ,  scale = c(6, 0.5) , max.words = 100, min.freq=2 ,  random.order = FALSE, colors=brewer.pal(8, "Set1"))
  saida
}
